import React from "react";
import '../App.css';

interface IProps {
  value: string | null;
  onClick: () => void;
}

export class Square extends React.Component<IProps> {

  render(): JSX.Element {
    return(
        <button
        className="square"
        onClick={this.props.onClick}
        >
          {this.props.value}
        </button>
    )
  }
}